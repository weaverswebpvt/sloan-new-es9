# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.


## 1.0.0 (2021-08-05)

### Features

**Initial release( Starting Production Level Coding Sloan )**

**Added Endpoints:**

  - /v1/auth ( *Auth Endpoints, Creating User Authentication* )
  - /v1/user ( *User Endpoints, Requires Authentication* )
  - /v1/webhook ( *Webhook Endpoints* )
  - /v1/sila ( *Sila Test Endpoints* )

**Added Services:**

  - *SILA*
  - *Twilio SMS*
  - *Sendgrid Add Contact & Automation, Emails*
  - *Response Templates Added*
  - *Message Template Added*
  - *Added Cron*
  - *Added Webhook*
  - *Added Sila Test Endpoints*
  - *Sila Module Seperated*
  - *Plaid Added*
  - *OneSignal Added*
  - *MethodFi*

**Onboarding:**

  - Social Signup
  - Manual Signup
  - Save Basic info
  - Send Otp
  - Verify Otp
  - Save Address ( **with SILA** )
  - Verify SSN ( **with SILA KYC, Sendgrid Contact Addition and Automation** )
  - Turn On Notifications ( **Onesignal yet to be added** )
  - Login with Email And Password ( **Tokens are Regenerated** )
  - Internal Login ( **Tokens are Regenerated** )
  - Get User Info
  - Forgot Password before Login( **Email will be sent out with 'App DeepLink URL' containing reset token in query to open the change password screen** )
  - Reset Password before Login
  - No KYC Flow Added ( **with SILA NO KYC, Sendgrid Contact Addition and Automation** )

  **Main:**

  - Banks Add, Get, Delete ( **Sila Bank Removal Added** )
  - Loans Add, Get, Delete
  - Plaid Loan Link Token
  - Plaid Bank Link Token
  - Plaid Exchange Public Token
  - Plaid Same Day MicroDeposit Verification Link Token
  - Plaid Verify Same Day MicroDeposit

  **Payment:**

  - Pay Loan From Bank ( **Webhook Pending** )
  - Started Working Pay Loan From Wallet

  **Webhooks**

  - Normal KYC Webhook Added ( **Working** )
  - Plaid Webhook Automated Microdeposit Verification ( **Webhook Pending** )
  - Payment Webhook (**PENDING WORK ON Check Webhook Body and use Transfer/Redeem SILA and DB update**)

**Response Templates:**

###### General Response

```json
{
  "code": <response code>,
  "message": <response message>,
  "details": {
    "fname": <first name>,
    "lname": <last name>,
    "Status": [Array],
    "address": [Array],
    "dob": <date of birth>,
    "profilePic": <profile picture>,
    "mobileNo": <phone number>,
    "studentId": <student Id>,
    "EmailId": <Email Id>,
  },
}
```

###### Full Response

```json
{
  "code": <response code>,
  "message": <response message>,
  "details": [object],
}
```

###### Full Response WIth Tokens

```json
{
  "code": <response code>,
  "message": <response message>,
  "details": [object],
  "tokens": {
      "access": {
          "token": <token>,
          "expires": <expiry>
      },
      "refresh": {
          "token": <token>,
          "expires": <expiry>
      }
  }
}
```

###### Bank Response

```json
{
  "code": <response code>,
  "message": <response message>,
  "details": [array of objects],
}
```

###### Loan Response

```json
{
  "code": <response code>,
  "message": <response message>,
  "details": [array of objects],
}
```