const Joi = require('joi');

const plaidWebhook = {
  body: Joi.object().required(),
};

const silaKYCWebhook = {
  body: Joi.object().required(),
};

module.exports = {
  plaidWebhook,
  silaKYCWebhook,
};
