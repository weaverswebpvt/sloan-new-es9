const Joi = require('joi');

const createVerifiedUser = {
  body: Joi.object().keys({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().required(),
    ipAddress: Joi.string().required(),
    type: Joi.string().required(),
    address1: Joi.string().required(),
    city: Joi.string().required(),
    state: Joi.string().required(),
    postalCode: Joi.string().required(),
    dateOfBirth: Joi.string().required(),
    ssn: Joi.string().required(),
  }),
};

module.exports = {
  createVerifiedUser,
};
