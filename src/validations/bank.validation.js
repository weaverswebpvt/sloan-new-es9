const Joi = require('joi');

const addBanks = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object(),
};

const getBanks = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const deleteBanks = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    accountName: Joi.string(),
  }),
};

module.exports = {
  addBanks,
  getBanks,
  deleteBanks,
};
