const Joi = require('joi');

// START SILA MANUAL CHECKS

const decryptData = {
  body: Joi.object().keys({
    data: Joi.string().required(),
  }),
};

const checkHandle = {
  body: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const checkkyc = {
  body: Joi.object().keys({
    walletKey: Joi.string().required(),
    userhandle: Joi.string().required(),
  }),
};

const updatePhone = {
  body: Joi.object().keys({
    walletKey: Joi.string().required(),
    phone: Joi.string().required(),
    userhandle: Joi.string().required(),
    uuid: Joi.string().required(),
  }),
};

const updateKyc = {
  body: Joi.object().keys({
    walletKey: Joi.string().required(),
    userhandle: Joi.string().required(),
    ssn: Joi.string().required(),
    uuid: Joi.string().required(),
  }),
};

const getStudent = {
  body: Joi.object().keys({
    walletKey: Joi.string().required(),
    userhandle: Joi.string().required(),
  }),
};

const getTransactions = {
  body: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const deleteTransaction = {
  body: Joi.object().keys({
    studentId: Joi.string().required(),
    tid: Joi.string().required(),
  }),
};

module.exports = {
  decryptData,
  checkHandle,
  checkkyc,
  updatePhone,
  updateKyc,
  getStudent,
  getTransactions,
  deleteTransaction,
};
