const Joi = require('joi');

const addLoans = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    loandata: Joi.object().required(),
  }),
};

const getLoans = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const deleteLoans = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    accountId: Joi.string().required(),
  }),
};

module.exports = {
  addLoans,
  getLoans,
  deleteLoans,
};
