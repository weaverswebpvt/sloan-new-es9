const Joi = require('joi');

const payLoanBank = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    bankName: Joi.string().required(),
    amount: Joi.string().required(),
    frequency: Joi.string().required(),
    loanAccount: Joi.string().required(),
    loanId: Joi.string().required(),
    loanName: Joi.string().required(),
    ins: Joi.string().required(),
    paymentType: Joi.string().required(),
    paymentFrom: Joi.string().required(),
  }),
};

const payLoanWallet = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    loanAccount: Joi.string().required(),
    amount: Joi.string().required(),
    loanId: Joi.string().required(),
    loanName: Joi.string().required(),
    paymentType: Joi.string().required(),
    paymentFrom: Joi.string().required(),
  }),
};

module.exports = {
  payLoanBank,
  payLoanWallet,
};
