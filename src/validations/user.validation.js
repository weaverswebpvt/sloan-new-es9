const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const createUser = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    name: Joi.string().required(),
    role: Joi.string().required().valid('user', 'admin'),
  }),
};

const getUsers = {
  query: Joi.object().keys({
    studentId: Joi.string(),
    fname: Joi.string(),
    lname: Joi.string(),
    EmailId: Joi.string().email(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const internalLogin = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const getUser = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const updateUser = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object()
    .keys({
      address1: Joi.string().required(),
      address2: Joi.string(),
      city: Joi.string().required(),
      state: Joi.string().required(),
      zip: Joi.string().required(),
    })
    .min(1),
};

const updateBasicInfo = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object()
    .keys({
      fname: Joi.string().required(),
      lname: Joi.string().required(),
      dob: Joi.string().required(),
    })
    .min(1),
};

const verifySSN = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object()
    .keys({
      ssn: Joi.string().required(),
    })
    .min(1),
};

const noKycFlow = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    dob: Joi.string().required(),
    phone: Joi.string().required(),
    address: Joi.object()
      .keys({
        address1: Joi.string().required(),
        address2: Joi.string(),
        city: Joi.string().required(),
        state: Joi.string().required(),
        zip: Joi.string().required(),
      })
      .min(1),
  }),
};

const sendOtp = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object()
    .keys({
      contact: Joi.string().required(),
    })
    .min(1),
};

const verifyOtp = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object()
    .keys({
      mobileNo: Joi.string().required(),
      otp: Joi.string().required(),
    })
    .min(1),
};

const turnNotificationsOn = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const addDeviceData = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const createNotification = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const deleteUser = {
  params: Joi.object().keys({
    userId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createUser,
  getUsers,
  internalLogin,
  getUser,
  updateUser,
  updateBasicInfo,
  verifySSN,
  noKycFlow,
  sendOtp,
  verifyOtp,
  turnNotificationsOn,
  addDeviceData,
  createNotification,
  deleteUser,
};
