const generalResponse = async (data, status, message) => {
  const response = {
    code: status,
    message,
    details: {
      fname: data.fname,
      lname: data.lname,
      Status: data.Status,
      address: data.address,
      dob: data.dob,
      profilePic: data.profilePic,
      mobileNo: data.mobileNo,
      studentId: data.studentId,
      EmailId: data.EmailId,
    },
  };
  return response;
};

const bankResponse = async (data, status, message) => {
  const response = {
    code: status,
    message,
    details: data.bankAccounts,
  };
  return response;
};

const fullResponse = async (data, status, message) => {
  const response = {
    code: status,
    message,
    details: data,
  };
  return response;
};

const loanResponse = async (data, status, message) => {
  const response = {
    code: status,
    message,
    details: data,
  };
  return response;
};

const fullResponseWithToken = async (data, tokens, status, message) => {
  const response = {
    code: status,
    message,
    details: data,
    tokens,
  };
  return response;
};

module.exports = {
  generalResponse,
  fullResponse,
  fullResponseWithToken,
  bankResponse,
  loanResponse,
};
