const Joi = require('joi');

const createLoanLinkToken = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const createBankLinkToken = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
};

const exchangePublicToken = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object(),
};

const createSameDayMicroDepositLink = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    accessToken: Joi.string().required(),
  }),
};

const verifySameDayMicroDeposit = {
  params: Joi.object().keys({
    studentId: Joi.string().required(),
  }),
  body: Joi.object().keys({
    accessToken: Joi.string().required(),
    accountId: Joi.string().required(),
    requestId: Joi.string().required(),
    itemId: Joi.string().required(),
  }),
};

module.exports = {
  createLoanLinkToken,
  createBankLinkToken,
  exchangePublicToken,
  createSameDayMicroDepositLink,
  verifySameDayMicroDeposit,
};
