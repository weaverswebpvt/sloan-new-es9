const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const { userValidation, loanValidation, bankValidation, plaidValidation, paymentValidation } = require('../../validations');
const { userController, bankController, loanController, plaidController, paymentController } = require('../../controllers');

const router = express.Router();

/**
 *
 * USER ONBOARDING FLOW
 * Includes: Onboarding flow, SILA Kyc Flow, No Kyc Flow, Phone Verification
 *
 */

router
  .route('/')
  .post(auth('manageUsers'), validate(userValidation.createUser), userController.createUser)
  .get(auth('getAllUsers'), validate(userValidation.getUsers), userController.getUsers);

router
  .route('/:studentId')
  .get(auth('internalLogin'), validate(userValidation.internalLogin), userController.internalLogin)
  .patch(auth('saveAddress'), validate(userValidation.updateUser), userController.saveAddress) // Update User Address
  .put(auth('saveBasicInfo'), validate(userValidation.updateBasicInfo), userController.saveBasicInfo) // Update User Basic Info Like Name, DOB
  .post(auth('verifySSN'), validate(userValidation.verifySSN), userController.verifySSN) // Using SILA KYC, Update User SSN
  .delete(auth('manageUsers'), validate(userValidation.deleteUser), userController.deleteUser); // Delete User

router.route('/:studentId/noKyc').post(auth('noKyc'), validate(userValidation.noKyc), userController.registerNoKyc); // Update User Data and use SILA NO KYC flow

router
  .route('/:studentId/otp')
  .post(auth('sendOtp'), validate(userValidation.sendOtp), userController.sendOtp) // Send OTP to User
  .patch(auth('verifyOtp'), validate(userValidation.verifyOtp), userController.verifyOtp); // Verify OTP and update User Phone

router
  .route('/:studentId/notificationsOn')
  .patch(auth('turnNotificationsOn'), validate(userValidation.turnNotificationsOn), userController.turnNotificationsOn); // Turn Notification ON

router.route('/:studentId/data').get(auth('getUser'), validate(userValidation.getUser), userController.getUser); // GET User Data
router
  .route('/:studentId/addDeviceData')
  .get(auth('addDeviceData'), validate(userValidation.addDeviceData), userController.addDeviceData); // GET User Data
router
  .route('/:studentId/createNotification')
  .get(auth('createNotification'), validate(userValidation.createNotification), userController.createNotification); // GET User Data

/**
 *
 * PLAID LINKING FLOW
 * Includes: Loan Link, Bank Link, Automated and Sameday Microdeposit Bank Verifcation
 *
 */

router
  .route('/:studentId/createLoanLinkToken')
  .get(auth('createLoanLinkToken'), validate(plaidValidation.createLoanLinkToken), plaidController.createLoanLinkToken); // Liabilities Link Token Creation Plaid

router
  .route('/:studentId/createBankLinkToken')
  .get(auth('createBankLinkToken'), validate(plaidValidation.createBankLinkToken), plaidController.createBankLinkToken); // Bank "Auth" Link Token Creation Plaid

router
  .route('/:studentId/exchangePublicToken')
  .post(auth('exchangePublicToken'), validate(plaidValidation.exchangePublicToken), plaidController.exchangePublicToken); // Bank Public Token Exchange Plaid & Save Automated Micro Deposits

router
  .route('/:studentId/createSameDayMicroDepositLink') // Bank Link Token Creation For User to enter Amounts for Same day Microdeposits Plaid
  .post(
    auth('createSameDayMicroDepositLink'),
    validate(plaidValidation.createSameDayMicroDepositLink),
    plaidController.createSameDayMicroDepositLink
  );

router
  .route('/:studentId/verifySameDayMicroDeposit') // Bank Verify Same day Microdeposits Plaid
  .post(
    auth('verifySameDayMicroDeposit'),
    validate(plaidValidation.verifySameDayMicroDeposit),
    plaidController.verifySameDayMicroDeposit
  );

/**
 *
 * BANKS AND LOANS
 * Includes: Addition, Fetch and deletion of Banks and Loans
 *
 */

router
  .route('/:studentId/banks')
  .get(auth('getBanks'), validate(bankValidation.getBanks), bankController.getBanks)
  .post(auth('addBanks'), validate(bankValidation.addBanks), bankController.addBanks)
  .delete(auth('deleteBanks'), validate(bankValidation.deleteBanks), bankController.deleteBanks);

router
  .route('/:studentId/loans')
  .get(auth('getLoans'), validate(loanValidation.getLoans), loanController.getLoans)
  .post(auth('addLoans'), validate(loanValidation.addLoans), loanController.addLoans)
  .delete(auth('deleteLoans'), validate(loanValidation.deleteLoans), loanController.deleteLoans);

/**
 *
 * Payments
 * Includes: Addfunds to wallet, Loan Payment By Wallet, Loan Payment By Bank
 *
 */

router
  .route('/:studentId/payLoanBank')
  .post(auth('payLoanBank'), validate(paymentValidation.payLoanBank), paymentController.payLoanBank);

router
  .route('/:studentId/payLoanWallet')
  .post(auth('payLoanWallet'), validate(paymentValidation.payLoanWallet), paymentController.payLoanWallet);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: User management and retrieval
 */

/**
 * @swagger
 * path:
 *  /users/{studentId}:
 *    get:
 *      summary: Internal Login
 *      description: Internally Logs the user in
 *      tags: [Users]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: studentId
 *          required: true
 *          schema:
 *            type: string
 *          description: User id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/User'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *        "500":
 *          $ref: '#/components/responses/InternalServerError'
 *
 *    patch:
 *      summary: Save User Address
 *      description: Logged in users can only update their Address.
 *      tags: [Users]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          address1: id
 *          required: true
 *          schema:
 *            type: string
 *          description: User id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                address1:
 *                  type: string
 *                city:
 *                  type: string
 *                state:
 *                  type: string
 *                zip:
 *                  type: string
 *              example:
 *                address1: fake address 1
 *                city: fake address 2
 *                state: fake address 2
 *                zip: password1
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/User'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *        "500":
 *          $ref: '#/components/responses/InternalServerError'
 *
 *    put:
 *      summary: Save User Basic Info
 *      description: Logged in users can save their basic info.
 *      tags: [Users]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          firstname: id
 *          lastname: id
 *          dob: id
 *          required: true
 *          schema:
 *            type: string
 *          description: User id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                fname:
 *                  type: string
 *                lname:
 *                  type: string
 *                dob:
 *                  type: string
 *              example:
 *                fname: fake name 1
 *                lname: fake name 2
 *                dob: fake dob
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/User'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *        "500":
 *          $ref: '#/components/responses/InternalServerError'
 *
 *    post:
 *      summary: Verify SSN
 *      description: Logged in users can Verify Their SSN using SILA KYC.
 *      tags: [Users]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          ssn: id
 *          required: true
 *          schema:
 *            type: string
 *          description: User id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                ssn:
 *                  type: string
 *              example:
 *                ssn: fake ssn
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/User'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *        "500":
 *          $ref: '#/components/responses/InternalServerError'
 */
