const express = require('express');
const validate = require('../../middlewares/validate');
const dwollaValidation = require('../../validations/dwolla.validation');
const dwollaController = require('../../controllers/dwolla.controller');

const router = express.Router();

router.post('/register', validate(dwollaValidation.createVerifiedUser), dwollaController.createVerifiedUser);

module.exports = router;
