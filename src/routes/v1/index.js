const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const docsRoute = require('./docs.route');
const webhookRoute = require('./webhook.route');
const silaRoute = require('./sila.route');
const dwollaRoute = require('./dwolla.route');

const router = express.Router();

router.use('/auth', authRoute);
router.use('/users', userRoute);
router.use('/docs', docsRoute);
router.use('/webhook', webhookRoute);
router.use('/sila', silaRoute);
router.use('/dwolla', dwollaRoute);

module.exports = router;
