const express = require('express');
const validate = require('../../middlewares/validate');
const silaValidation = require('../../validations/sila.validation');
const silaController = require('../../controllers/sila.controller');

const router = express.Router();

// START SILA MANUAL CHECKS
router.post('/decrypt-data', validate(silaValidation.decryptData), silaController.decryptData);
router.post('/checkhandle', validate(silaValidation.checkHandle), silaController.checkHandle);
router.post('/checkkyc', validate(silaValidation.checkkyc), silaController.checkkyc);
router.post('/updateKyc', validate(silaValidation.updateKyc), silaController.updateKyc);
router.post('/updatePhone', validate(silaValidation.updatePhone), silaController.updatePhone);
router.post('/getStudent', validate(silaValidation.getStudent), silaController.getStudent);
router.post('/getTransactions', validate(silaValidation.getTransactions), silaController.getTransactions);
router.post('/deleteTransaction', validate(silaValidation.deleteTransaction), silaController.deleteTransaction);

module.exports = router;
