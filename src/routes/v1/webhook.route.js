const express = require('express');
const validate = require('../../middlewares/validate');
const webhookValidation = require('../../validations/webhook.validation');
const webhookController = require('../../controllers/webhook.controller');

const router = express.Router();

/**
 *
 * Plaid Webhook
 * Includes: Automated MicroDeposits
 *
 */

router.post('/plaidWebhook', validate(webhookValidation.plaidWebhook), webhookController.plaidWebhook);
router.post('/silaKYCWebhook', validate(webhookValidation.silaKYCWebhook), webhookController.silaKYCWebhook);

module.exports = router;
