const { version } = require('../../package.json');
const config = require('../config/config');

const swaggerDef = {
  openapi: '3.0.0',
  info: {
    title: 'Sloan v2 Release 1.0.0',
    version,
    license: {
      name: 'MIT',
      url: 'https://bitbucket.org/weaverswebpvt/sloan-new-es9.git',
    },
  },
  servers: [
    {
      url: `http://localhost:${config.port}/v1`,
    },
  ],
};

module.exports = swaggerDef;
