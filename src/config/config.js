const dotenv = require('dotenv');
const path = require('path');
const Joi = require('joi');

dotenv.config({ path: path.join(__dirname, '../../.env') });

const envVarsSchema = Joi.object()
  .keys({
    NODE_ENV: Joi.string().valid('production', 'development', 'test').required(),
    PORT: Joi.number().default(3000),
    MONGODB_URL: Joi.string().required().description('Mongo DB url'),
    JWT_SECRET: Joi.string().required().description('JWT secret key'),
    JWT_ACCESS_EXPIRATION_MINUTES: Joi.number().default(30).description('minutes after which access tokens expire'),
    JWT_REFRESH_EXPIRATION_DAYS: Joi.number().default(30).description('days after which refresh tokens expire'),
    SMTP_HOST: Joi.string().description('server that will send the emails'),
    SMTP_PORT: Joi.number().description('port to connect to the email server'),
    SMTP_USERNAME: Joi.string().description('username for email server'),
    SMTP_PASSWORD: Joi.string().description('password for email server'),
    EMAIL_FROM: Joi.string().description('the from field in the emails sent by the app'),
    SILA_HANDLE: Joi.string().description('the Sila Handle'),
    SILA_STAGING_HANDLE: Joi.string().description('the Sila Handle'),
    SILA_KEY: Joi.string().description('the Live Sila key'),
    SILA_STAGING_KEY: Joi.string().description('the Live Sila key'),
    SILA_BUSINESS_UUID: Joi.string().description('sila business uuid'),
    FBO_HANDLE: Joi.string().description('the FBO Handle'),
    FBO_WALLET_KEY: Joi.string().description('the FBO Wallet Key'),
    FBO_WALLET_ADDRESS: Joi.string().description('the FBO Handle'),
    FEE_HANDLE: Joi.string().description('the FEE Handle'),
    FEE_WALLET_KEY: Joi.string().description('the FEE Wallet Key'),
    FEE_WALLET_ADDRESS: Joi.string().description('the FEE Wallet Address'),
    RESERVE_HANDLE: Joi.string().description('the RESERVE Handle'),
    RESERVE_WALLET_KEY: Joi.string().description('the RESERVE Wallet Key'),
    RESERVE_WALLET_ADDRESS: Joi.string().description('the RESERVE Wallet Address'),
    SGAPI: Joi.string().description('Sendgrid API Key'),
    TWILIO_SID: Joi.string().description('Twilio Account SID'),
    TWILIO_AUTH: Joi.string().description('Twilio Auth Token'),
    PLAID_CLIENT_ID: Joi.string().description('Plaid Production Client ID'),
    PLAID_SECRET: Joi.string().description('Plaid Producttion Secret'),
    PLAID_ENV: Joi.string().description('Plaid Production Env'),
    PLAID_CLIENT_ID_SANDBOX: Joi.string().description('Plaid Sandbox Client ID'),
    PLAID_SECRET_SANDBOX: Joi.string().description('Plaid Sandox Secret'),
    PLAID_ENV_SANDBOX: Joi.string().description('Plaid Sandbox Env'),
    DWOLLA_KEY_SANDBOX: Joi.string().description('Dwolla Client Key Env'),
    DWOLLA_SECRET_SANDBOX: Joi.string().description('Dwolla Client Secret Env'),
    DWOLLA_KEY: Joi.string().description('Dwolla Client Key Env'),
    DWOLLA_SECRET: Joi.string().description('Dwolla Client Secret Env'),
    METHODFIAPI: Joi.string().description('METHODFI API'),
  })
  .unknown();

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: 'key' } }).validate(process.env);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

module.exports = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  mongoose: {
    url: envVars.MONGODB_URL + (envVars.NODE_ENV === 'test' ? '-test' : ''),
    options: {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },
  jwt: {
    secret: envVars.JWT_SECRET,
    accessExpirationMinutes: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: envVars.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes: 30,
  },
  email: {
    smtp: {
      host: envVars.SMTP_HOST,
      port: envVars.SMTP_PORT,
      auth: {
        user: envVars.SMTP_USERNAME,
        pass: envVars.SMTP_PASSWORD,
      },
    },
    from: envVars.EMAIL_FROM,
    sendgridApi: envVars.SGAPI,
  },
  twilio: {
    accountSid: envVars.TWILIO_SID,
    authToken: envVars.TWILIO_AUTH,
  },
  sila_staging: {
    handle: envVars.SILA_STAGING_HANDLE,
    key: envVars.SILA_STAGING_KEY,
    business_uuid: envVars.SILA_BUSINESS_UUID,
  },
  sila: {
    handle: envVars.SILA_HANDLE,
    key: envVars.SILA_KEY,
    business_uuid: envVars.SILA_BUSINESS_UUID,
  },
  dwolla: {
    sandbox: {
      key: envVars.DWOLLA_KEY_SANDBOX,
      secret: envVars.DWOLLA_SECRET_SANDBOX,
    },
    production: {
      key: envVars.DWOLLA_KEY,
      secret: envVars.DWOLLA_SECRET,
    },
  },
  plaid: {
    production: {
      client_id: envVars.PLAID_CLIENT_ID,
      secret: envVars.PLAID_SECRET,
    },
    sandbox: {
      client_id: envVars.PLAID_CLIENT_ID_SANDBOX,
      secret: envVars.PLAID_SECRET_SANDBOX,
    },
  },
  fboaccount: {
    handle: envVars.FBO_HANDLE,
    walletKey: envVars.FBO_WALLET_KEY,
    walletAddress: envVars.FBO_WALLET_ADDRESS,
  },
  feeaccount: {
    handle: envVars.FEE_HANDLE,
    walletKey: envVars.FEE_WALLET_KEY,
    walletAddress: envVars.FEE_WALLET_ADDRESS,
  },
  reserve: {
    handle: envVars.RESERVE_HANDLE,
    walletKey: envVars.RESERVE_WALLET_KEY,
    walletAddress: envVars.RESERVE_WALLET_ADDRESS,
  },
  methodfi: {
    handle: envVars.METHODFI_HANDLE,
    walletAddress: envVars.METHODFI_WALLET_KEY,
    walletKey: envVars.METHODFI_WALLET_ADDRESS,
    source: envVars.METHODFI_SOURCE_FBO,
    api: envVars.METHODFI_API,
  },
};
