module.exports.Token = require('./token.model');
module.exports.User = require('./user.model');
module.exports.Otp = require('./otp.model');
module.exports.Payment = require('./payment.model');
module.exports.Recurring = require('./recurring.model');
module.exports.Roundup = require('./roundup.model');
module.exports.Loan = require('./loan.model');
module.exports.Bank = require('./bank.model');
