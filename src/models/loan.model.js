const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const loanSchema = mongoose.Schema(
  {
    studentId: {
      type: String,
      required: true,
      index: true,
    },
  },
  {
    strict: false,
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
loanSchema.plugin(toJSON);

/**
 * @typedef loan
 */
const loan = mongoose.model('Loan', loanSchema);

module.exports = loan;
