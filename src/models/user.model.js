const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { toJSON, paginate } = require('./plugins');

const userSchema = mongoose.Schema(
  {
    studentId: {
      type: String,
      required: true,
      trim: true,
    },
    fname: {
      type: String,
      required: false,
      trim: true,
    },
    lname: {
      type: String,
      required: false,
      trim: true,
    },
    EmailId: {
      type: String,
      required: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Invalid email');
        }
      },
    },
    deviceId: {
      type: String,
      required: false,
      trim: true,
    },
    salt: {
      type: String,
      required: true,
      trim: true,
    },
    pushToken: {
      type: String,
      required: false,
      trim: true,
    },
    deviceType: {
      type: String,
      required: false,
      trim: true,
    },
    password: {
      type: String,
      required: false,
      trim: true,
      minlength: 8,
      validate(value) {
        if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
          throw new Error('Password must contain at least one letter and one number');
        }
      },
      private: true, // used by the toJSON plugin
    },
    Status: {
      signedUpVia: {
        type: String,
        trim: true,
      },
      isVerified: {
        type: Boolean,
        trim: true,
      },
      doNotifications: {
        type: Boolean,
        trim: true,
      },
      isOnline: {
        type: Boolean,
        trim: true,
      },
      fcmTocken: {
        type: String,
        trim: true,
      },
      authTocken: {
        type: String,
        trim: true,
      },
      socialId: {
        type: String,
        trim: true,
      },
      isKYCVerified: {
        type: Boolean,
        default: false,
      },
    },
    address: {
      address1: {
        type: String,
        default: '',
      },
      address2: {
        type: String,
        default: '',
      },
      city: {
        type: String,
        default: '',
      },
      state: {
        type: String,
        default: '',
      },
      zip: {
        type: String,
        default: '',
      },
    },
    dob: {
      type: String,
      default: '',
    },
    profilePic: {
      type: String,
      default: '',
    },
    mobileNo: {
      type: String,
      default: '',
    },
    lastSendOtp: {
      type: String,
      trim: true,
    },
    memberSince: {
      type: String,
      trim: true,
    },
    completionStatus: {
      type: String,
      trim: true,
    },
    walletKey: {
      type: String,
      trim: true,
    },
    walletAddress: {
      type: String,
      trim: true,
    },
    walletKeySecond: {
      type: String,
      trim: true,
    },
    walletAddressSecond: {
      type: String,
      trim: true,
    },
    customToken: {
      type: String,
      trim: true,
    },
    loanDetails: [],
    bankAccounts: [],
    roundUps: [],
    school: {
      type: String,
      trim: true,
    },
    gradschool: {
      type: String,
      trim: true,
    },
    activities: [],
    about: {
      type: String,
      trim: true,
    },
    sloanbalance: {
      type: String,
      trim: true,
    },
    roundUpACHlast: {
      type: String,
      trim: true,
    },
    isLoggedIn: {
      type: Boolean,
      trim: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
userSchema.plugin(toJSON);
userSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
userSchema.statics.isEmailTaken = async function (EmailId, excludeUserId) {
  const user = await this.findOne({ EmailId, _id: { $ne: excludeUserId } });
  return !!user;
};

/**
 * Check if password matches the user's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
userSchema.methods.isPasswordMatch = async function (password) {
  const user = this;
  return bcrypt.compare(password, user.password);
};

userSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

/**
 * @typedef User
 */
const User = mongoose.model('User', userSchema);

module.exports = User;
