const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const recurringSchema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    studentId: String,
    amount: {
      type: Number,
      required: true,
    },
    bankId: {
      type: String,
      required: true,
    },
    bankName: {
      type: String,
      required: true,
    },
    accountNum: {
      type: String,
      required: true,
    },
    accountRouting: {
      type: String,
      required: true,
    },
    accountType: {
      type: String,
      required: true,
    },
    accountClass: {
      type: String,
      required: true,
    },
    loanId: {
      type: String,
      required: true,
    },
    loanname: {
      type: String,
      required: true,
    },
    loanAccount: {
      type: String,
      required: true,
    },
    frequency: {
      type: String,
      required: true,
    },
    next_payment_date: {
      type: String,
      required: true,
    },
    paymentId: {
      type: String,
      required: true,
    },
    loan_ins: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
recurringSchema.plugin(toJSON);

/**
 * @typedef recurring
 */
const recurring = mongoose.model('Recurring', recurringSchema);

module.exports = recurring;
