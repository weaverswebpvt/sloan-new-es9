const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const roundUpSchema = mongoose.Schema(
  {
    _id: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    studentId: {
      type: String,
      required: true,
    },
    flag: {
      type: String,
      required: true,
    },
    account_id: {
      type: String,
      required: true,
    },
    accountnum: {
      type: String,
      required: true,
    },
    account_owner: {
      type: String,
      required: true,
    },
    amount: {
      type: String,
      required: true,
    },
    roundedupAmount: {
      type: String,
      required: true,
    },
    roundup: {
      type: String,
      required: true,
    },
    authorized_date: {
      type: String,
      required: true,
    },
    category: {
      type: String,
      required: true,
    },
    category_id: {
      type: String,
      required: true,
    },
    date: {
      type: String,
      required: true,
    },
    iso_currency_code: {
      type: String,
      required: true,
    },
    location: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    payment_channel: {
      type: String,
      required: true,
    },
    payment_meta: {
      type: String,
      required: true,
    },
    pending: {
      type: String,
      required: true,
    },
    pending_transaction_id: {
      type: String,
      required: true,
    },
    transaction_code: {
      type: String,
      required: true,
    },
    transaction_id: {
      type: String,
      required: true,
    },
    transaction_type: {
      type: String,
      required: true,
    },
    unofficial_currency_code: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
roundUpSchema.plugin(toJSON);

/**
 * @typedef roundUp
 */
const roundUp = mongoose.model('RoundUp', roundUpSchema);

module.exports = roundUp;
