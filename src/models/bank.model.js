const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const bankSchema = mongoose.Schema(
  {
    studentId: {
      type: String,
      required: true,
      index: true,
    },
    access_token: {
      type: String,
      index: true,
      required: true,
    },
    requestId: {
      type: String,
      index: true,
    },
  },
  {
    strict: false,
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
bankSchema.plugin(toJSON);

/**
 * @typedef bank
 */
const bank = mongoose.model('Bank', bankSchema);

module.exports = bank;
