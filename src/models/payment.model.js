const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const paymentSchema = mongoose.Schema(
  {
    studentId: {
      type: String,
      required: true,
      index: true,
    },
    paymentId: {
      type: String,
      required: true,
      index: true,
    },
    tId: {
      type: String,
      required: true,
      index: true,
    },
    amount: {
      type: String,
      required: true,
    },
    bankName: {
      type: String,
      required: true,
    },
    loanId: {
      type: String,
      required: true,
    },
    loanAccount: {
      type: String,
      required: true,
    },
    frequency: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    loan_ins: {
      type: String,
      required: true,
    },
    paymentType: {
      type: String,
      required: true,
    },
    paymentFrom: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
paymentSchema.plugin(toJSON);

/**
 * @typedef payment
 */
const payment = mongoose.model('Payment', paymentSchema);

module.exports = payment;
