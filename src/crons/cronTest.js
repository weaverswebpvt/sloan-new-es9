const cron = require('node-cron');
const logger = require('../config/logger');

const task = cron.schedule('* * * * * *', () => {
  logger.info('Running Task Every Second');
});

const taskTest = cron.schedule('* * * * * *', () => {
  logger.info('Running New Task Every Second');
});

module.exports = {
  task,
  taskTest,
};
