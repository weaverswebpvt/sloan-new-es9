const responseMessages = async (response) => {
  const messages = {
    createUser: 'Successfull',
    getUsers: 'SUccessfully Retieved Users',
    getUser: 'Successfully Retieved User',
    login: 'You have been successfully logged in.',
    internalLogin: 'Internal Login Successfull.',
    forgotPassword: 'An email has been send with a link. Click the link to proceed',
    resetPassword: 'Password Changed successfully',
    alreadyRegistered: 'You are already registered',
    basicInfoStored: 'Basic Information is successfully inserted',
    addressStored: 'Address is successfully inserted',
    ssnVerified: 'SSN Verified Successfully',
    notificationsOn: 'You have successfully make notification on.',
    otpSent: 'Otp sent.Otp will valid for 5 minutes',
    phoneVerified: 'Phone number is verified ',
    passwordChanged: 'Password has changed successfully',
    institueExists: 'Institute already exists',
    wrongPassword: 'Wrong Password,Please try again',
    notRegistered: 'You are not registered',
    notificationsOff: 'You have turned off notification',
    otpExpired: 'Otp has expired',
    otpMismatch: 'Otp does not match',
    bankAdded: 'The Bank has been successfully added',
    getBanks: 'Banks Retrived successfully',
    bankDeleted: 'The Bank has been successfully removed',
    loanAdded: 'The Loan has been successfully added',
    getLoans: 'Loans Retrived successfully',
    loanDeleted: 'The Loan has been successfully removed',
    paymentMade:
      'The payment has been send successfully, please wait for 2 - 3 days for the amount to get transfered to you loan',
  };

  return messages[response];
};

module.exports = responseMessages;
