const httpStatus = require('http-status');
const Sila = require('sila-sdk').default;
const moment = require('moment');
const ApiError = require('../utils/ApiError');
const userService = require('./user.service');
const silaService = require('./sila.service');
const { Payment } = require('../models');
const config = require('../config/config');

const silaconfig = {
  handle: config.sila_staging.handle,
  key: config.sila_staging.key,
};
Sila.configure(silaconfig);

const payLoanBank = async (studentId, req) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const timeAnddate = `${new Date().getTime()}-${Math.random().toString(36).replace('0.', '')}`;
  let paymentFrequency;
  if (req.frequency === 'Weekly') {
    paymentFrequency = moment().add(7, 'days').format('YYYY-MM-DD');
  } else if (req.frequency === 'Monthly') {
    paymentFrequency = moment().add(1, 'months').format('YYYY-MM-DD');
  } else {
    paymentFrequency = 'One time';
  }
  const wpk = await userService.decryptKeys(user.walletKey);
  const response = await silaService.issueSila(studentId, req.amount, wpk, req.bankName);
  const insertBody = {
    studentId,
    paymentId: timeAnddate,
    amount: req.amount,
    bankName: req.bankName,
    frequency: paymentFrequency,
    loanAccount: req.loanAccount,
    loanId: req.loanId,
    loanName: req.loanName,
    loan_ins: req.ins,
    paymentType: req.paymentType,
    paymentFrom: req.paymentFrom,
    tId: response.data.transaction_id,
    status: 'issue',
  };
  const payment = await Payment.create(insertBody);
  return payment;
};

const payLoanWallet = async (studentId) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const loans = await userService.getLoansById(studentId);
  return loans;
};

module.exports = {
  payLoanWallet,
  payLoanBank,
};
