const nodemailer = require('nodemailer');
const sgMail = require('@sendgrid/mail');
const sgClient = require('@sendgrid/client');
const config = require('../config/config');
const logger = require('../config/logger');

sgMail.setApiKey(config.email.sendgridApi);
sgClient.setApiKey(config.email.sendgridApi);

const transport = nodemailer.createTransport(config.email.smtp);
/* istanbul ignore next */
if (config.env !== 'test') {
  transport
    .verify()
    .then(() => logger.info('Connected to email server'))
    .catch(() => logger.warn('Unable to connect to email server. Make sure you have configured the SMTP options in .env'));
}

/**
 * Send an email
 * @param {string} to
 * @param {string} subject
 * @param {string} text
 * @returns {Promise}
 */
const sendEmail = async (to, subject, text) => {
  const msg = { from: config.email.from, to, subject, text };
  await transport.sendMail(msg);
};

const addContactSendgrid = async (
  firstName,
  lastName,
  email,
  addressLine1,
  city,
  stateProvinceRegion,
  postalCode,
  phoneNumber,
  studentId
) => {
  const request = {
    method: 'PUT',
    url: '/v3/marketing/contacts',
    body: {
      list_ids: ['8557e121-3084-46f2-aa15-1a4b2ec5a26c'],
      contacts: [
        {
          first_name: firstName,
          last_name: lastName,
          email,
          address_line_1: addressLine1,
          city,
          state_province_region: stateProvinceRegion,
          postal_code: postalCode,
          phone_number: phoneNumber,
          unique_name: studentId,
        },
      ],
    },
  };
  return sgClient.request(request);
};

/** Send an email with Sendgrid using Templates
 * @param {string} to
 * @param {string} subject
 * @param {string} text
 * @returns {Promise}
 */
const sendEmailWithSendgridTemplated = async (to, dynamicTemplateData, templateId) => {
  const msg = {
    to,
    from: 'arindam.bhattacharyya@pkweb.in',
    templateId,
    dynamicTemplateData,
  };
  return sgMail.send(msg);
};

/** Send an email with Sendgrid ( No Use Of Templates)
 * @param {string} to
 * @param {string} subject
 * @param {string} text
 * @returns {Promise}
 */
const sendEmailWithSendgrid = async (to, subject, text) => {
  const msg = {
    to,
    from: 'arindam.bhattacharyya@pkweb.in', // Use the email address or domain you verified above
    subject,
    text,
  };
  return sgMail.send(msg);
};

/**
 * Send reset password email
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendResetPasswordEmail = async (to, subject, token) => {
  const resetPasswordUrl = `http://link-to-app/reset-password?token=${token}`;
  const text = `Dear user,
  To reset your password, click on this link: ${resetPasswordUrl}
  If you did not request any password resets, then ignore this email.`;
  const msg = {
    to,
    from: 'arindam.bhattacharyya@pkweb.in', // Use the email address or domain you verified above
    subject,
    text,
  };
  return sgMail.send(msg);
};

module.exports = {
  transport,
  sendEmail,
  addContactSendgrid,
  sendResetPasswordEmail,
  sendEmailWithSendgrid,
  sendEmailWithSendgridTemplated,
};
