/*eslint-disable*/
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const userService = require('./user.service');
const { Loan } = require('../models');
const config = require('../config/config');

const { Method, Environments } = require('method-node');

const methodclient = new Method({
  apiKey: config.methodfi.api,
  env: Environments.sandbox,
});

const checkMethod = async () => {
  let check = await methodclient.ping();
  console.log(check);
  return check;
};

const createUser = async (req) => {
  const entity = await methodclient.entities.create({
    type: 'individual',
    individual: {
      first_name: req.first_name,
      last_name: req.last_name,
      phone: req.phone,
      email: req.email,
      dob: req.dob,
    },
    address: {
      line1: req.line1,
      line2: req.line2,
      city: req.city,
      state: req.state,
      zip: req.zip,
    },
  });
  if (entity.hasOwnProperty('id')) {
    console.log(entity);
    return entity.id;
  } else {
    console.log(entity);
  }
};

const linkAcc = async (req) => {
  const account = await methodclient.accounts.create({
    holder_id: req.account_holder_id,
    ach: {
      routing: req.routing,
      number: req.number,
      type: req.type,
    },
  });
  if (account.hasOwnProperty('id')) {
    return account.id;
  }
};

const linkLoan = async (req) => {
  const account = await methodclient.accounts.create({
    holder_id: req.account_holder_id,
    liability: {
      mch_id: req.mch_id,
      account_number: req.account_number,
    },
  });
  console.log(req);
  console.log(account);
  if (account.hasOwnProperty('id')) {
    return account.id;
  }
};

const loanPayment = async (req) => {
  const payment = await methodclient.payments.create({
    amount: req.amount,
    source: req.source,
    destination: req.destination,
    description: 'Loan Pmt',
  });
  if (payment.hasOwnProperty('id')) {
    return payment;
  }
};

const getMerchantByIns = async (ins) => {
  const payment = await methodclient.merchants.list({
    'provider_id.plaid': ins,
  });

  console.log(payment);
  if (payment) {
    return payment[0].mch_id;
  }
};

module.exports = {
  checkMethod,
  createUser,
  linkAcc,
  linkLoan,
  loanPayment,
  getMerchantByIns,
};
