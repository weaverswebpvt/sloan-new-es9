const httpStatus = require('http-status');
const Sila = require('sila-sdk').default;
const ApiError = require('../utils/ApiError');
const config = require('../config/config');
const logger = require('../config/logger');

const silaconfig = {
  handle: config.sila_staging.handle,
  key: config.sila_staging.key,
};
Sila.configure(silaconfig);

// START SILA

const checkHandle = async (studentId) => {
  const userhandle = `${studentId}.silamoney.eth`;
  const response = await Sila.checkHandle(userhandle);
  return response;
};

const checkkyc = async (req) => {
  try {
    const wpk = req.walletKey;
    const response = await Sila.checkKYC(req.userhandle, wpk);
    return response;
  } catch (e) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, e);
  }
};

const updateKyc = async (req) => {
  try {
    const wpk = req.walletKey;
    const identity = {
      alias: 'SSN',
      value: req.ssn,
      uuid: req.uuid,
    };
    const response = await Sila.addIdentity(req.userhandle, wpk, identity);
    return response;
  } catch (e) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, e);
  }
};

const updatePhone = async (req) => {
  try {
    const wpk = req.walletKey;
    const phone = {
      phone: req.phone,
      uuid: req.uuid,
      smsOptIn: true,
    };
    const response = await Sila.updatePhone(req.userhandle, wpk, phone);
    return response;
  } catch (e) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, e);
  }
};

const getStudent = async (req) => {
  try {
    const wpk = req.walletKey;
    const response = await Sila.getEntity(req.userhandle, wpk);
    return response;
  } catch (e) {
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, e);
  }
};

const getBankAccounts = async (handle, wpk) => {
  const response = await Sila.getAccounts(handle, wpk);
  return response.data;
};

const linkBankSila = async (req) => {
  const getbanks = await getBankAccounts(req.userhandle, req.walletKey);
  const account = getbanks.find((acc) => acc.account_name === req.accountName);
  if (account) {
    throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, `Bank Account ${req.accountName} already exists.`);
  }
  const response = await Sila.linkAccountDirect(
    req.userhandle,
    req.walletKey,
    req.accountNumber,
    req.routingNumber,
    req.accountName,
    req.accountType
  );
  if (response.statusCode !== 200) {
    throw new ApiError(
      httpStatus.UNPROCESSABLE_ENTITY,
      `Bank Account ${req.accountName} could not be connected. It returned status ${response.statusCode}`
    );
  }
  return response;
};

const issueSila = async (studentId, amonut, wpk, bankName) => {
  const userHandle = `${studentId}.silamoney.eth`;
  const SilaAmount = parseFloat(amonut) * 100 + parseFloat(199); // $1.99 fees added to issue Sila
  const response = await Sila.issueSila(SilaAmount, userHandle, wpk, bankName); // config.sila_staging.business_uuid needed for production
  logger.info('ISSUE SILA RESPONSE > %s', response.data);
  if (response.statusCode !== 200) {
    throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, `Payment not processed. It returned status ${response.statusCode}`);
  }
  return response;
};

const transferSila = async (amount, studentId, wpk, destination, destinationWallet, destinationAddress, fees) => {
  const userHandle = `${studentId}.silamoney.eth`;
  let response;
  if (fees) {
    const SilaAmount = parseFloat(amount) * 100 - parseFloat(199);
    response = await Sila.transferSila(SilaAmount, userHandle, wpk, destination, destinationWallet, destinationAddress);
    logger.info('TRANSFER SILA RESPONSE > %s', response.data);
  } else {
    const SilaAmount = parseFloat(amount) * 100;
    response = await Sila.transferSila(SilaAmount, userHandle, wpk, destination, destinationWallet, destinationAddress);
    logger.info('TRANSFER SILA RESPONSE > %s', response.data);
  }
  if (response.statusCode !== 200) {
    throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, `Transfer not processed. It returned status ${response.statusCode}`);
  }
  return response;
};

const deleteBankSila = async (studentId, accountName, walletKey) => {
  const userhandle = `${studentId}.silamoney.eth`;
  const getbanks = await getBankAccounts(userhandle, walletKey);
  const account = getbanks.find((acc) => acc.account_name === accountName);
  if (!account) {
    throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, `Bank Account ${accountName} does not exist.`);
  }
  const response = await Sila.deleteAccount(userhandle, accountName, walletKey);
  if (response.statusCode !== 200) {
    throw new ApiError(
      httpStatus.UNPROCESSABLE_ENTITY,
      `Bank Account ${accountName} could not be deleted. It returned status ${response.statusCode}`
    );
  }
  return response.data;
};

const getTransactions = async (studentId, wpk) => {
  const userHandle = `${studentId}.silamoney.eth`;
  const response = await Sila.getTransactions(userHandle, wpk);
  return response;
};

const deleteTransaction = async (studentId, wpk, tid) => {
  const userHandle = `${studentId}.silamoney.eth`;
  const response = await Sila.cancelTransaction(userHandle, wpk, tid);
  return response;
};

module.exports = {
  checkHandle,
  checkkyc,
  updateKyc,
  getBankAccounts,
  updatePhone,
  getStudent,
  linkBankSila,
  issueSila,
  transferSila,
  deleteBankSila,
  getTransactions,
  deleteTransaction,
};
