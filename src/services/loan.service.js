const httpStatus = require('http-status');
const Sila = require('sila-sdk').default;
const ApiError = require('../utils/ApiError');
const userService = require('./user.service');
const { Loan } = require('../models');
const config = require('../config/config');

const silaconfig = {
  handle: config.sila_staging.handle,
  key: config.sila_staging.key,
};
Sila.configure(silaconfig);

const getLoans = async (studentId) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const loans = await userService.getLoansById(studentId);
  return loans;
};

const addLoans = async (studentId, insertBody) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const {
    account_id: accountId,
    account_number: accountNumber,
    disbursement_dates: disbursementDates,
    expected_payoff_date: expectedPayoffDate,
    guarantor,
    interest_rate_percentage: interestRatePercentage,
    is_overdue: isOverdue,
    last_payment_amount: lastPaymentAmount,
    last_payment_date: lastPaymentDate,
    loan_name: loanName,
    loan_status: loanStatus,
    minimum_payment_amount: minimumPaymentAmount,
    next_payment_due_date: nextPaymentDueDate,
    origination_date: originationDate,
    origination_principal_amount: originationPrincipalAmount,
    account,
    item,
    access_token: accessToken,
  } = insertBody;

  const loanData = {
    studentId,
    accountId,
    accountNumber,
    loanName,
    disbursementDates,
    expectedPayoffDate,
    guarantor: guarantor.trim(),
    interestRatePercentage,
    isOverdue,
    lastPaymentAmount,
    lastPaymentDate,
    loanStatus,
    minimumPaymentAmount,
    nextPaymentDueDate,
    originationDate,
    originationPrincipalAmount,
    balanceAvailable: account.balances.available,
    balanceCurrent: account.balances.current,
    mask: account.mask,
    maskName: account.name,
    institutionId: item.institution_id,
    itemId: item.item_id,
    accessToken,
  };
  await Loan.create(loanData);
  const getLoanUser = await userService.getLoansById(studentId);
  return getLoanUser;
};

const deleteLoans = async (studentId, accountId) => {
  const user = await userService.getUserById(studentId);
  const loan = await userService.getLoansById(accountId);
  if (!loan) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Loan not found');
  }
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const response = await Loan.findOneAndRemove(accountId);
  return response;
};

module.exports = {
  getLoans,
  addLoans,
  deleteLoans,
};
