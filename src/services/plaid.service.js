const httpStatus = require('http-status');
const plaid = require('plaid');
const userService = require('./user.service');
const bankService = require('./bank.service');
const ApiError = require('../utils/ApiError');
const config = require('../config/config');
const logger = require('../config/logger');

const plaidClient = new plaid.Client({
  clientID: config.plaid.sandbox.client_id, // change sandbox to production
  secret: config.plaid.sandbox.secret, // change sandbox to production
  env: plaid.environments.sandbox, // change sandbox to production
});

const createLoanLinkToken = async (studentId) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const linkTokenResponse = await plaidClient.createLinkToken({
    user: {
      client_user_id: studentId,
    },
    client_name: 'Sloan',
    products: ['liabilities'],
    country_codes: ['US'],
    language: 'en',
    link_customization_name: 'loans',
    webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
  });
  return linkTokenResponse;
};

const createBankLinkToken = async (studentId) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const linkTokenResponse = await plaidClient.createLinkToken({
    user: {
      client_user_id: studentId,
    },
    client_name: 'Sloan',
    products: ['auth'],
    country_codes: ['US'],
    language: 'en',
    webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
  });
  return linkTokenResponse;
};

const exchangePublicToken = async (studentId, requestBody) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const linkTokenResponse = await plaidClient.exchangePublicToken(requestBody.publicToken);
  logger.info('Public Token Response %s', JSON.stringify(linkTokenResponse));
  const { accountId, status } = requestBody;
  linkTokenResponse.accountId = accountId;
  linkTokenResponse.status = status;
  const saveBank = await bankService.saveBankPublicToken(studentId, linkTokenResponse);
  return saveBank;
};

const retrieveAuth = async (accessToken) => {
  const authResponse = await plaidClient.getAuth(accessToken).catch((err) => {
    // Indicates a network or runtime error.
    if (!(err instanceof plaid.PlaidError)) {
      throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, 'Plaid Error');
    }
    // Indicates plaid API error
    logger.info('/Retrieve Auth %s', {
      error_type: err.error_type,
      error_message: err.error_message,
      display_message: err.display_message,
      request_id: err.request_id,
      status_code: err.status_code,
    });
  });
  return authResponse;
};

const retrieveAccount = async (accessToken, accountId) => {
  const authResponse = await plaidClient
    .getAccounts(accessToken, {
      account_ids: [accountId],
    })
    .catch((err) => {
      // Indicates a network or runtime error.
      if (!(err instanceof plaid.PlaidError)) {
        throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, 'Plaid Error');
      }
      // Indicates plaid API error
      logger.info('/Retrieve Account %s', {
        error_type: err.error_type,
        error_message: err.error_message,
        display_message: err.display_message,
        request_id: err.request_id,
        status_code: err.status_code,
      });
    });
  return authResponse;
};

/**
 *
 *
 * Verify Automated Micro Deposits
 *
 *
 */
const verifyAutomatedMicroDeposits = async (data) => {
  const getBank = await bankService.getBanksByItemId(data.item_id);
  if (!getBank) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Bank not found');
  }
  const authResponse = await retrieveAuth(getBank.access_token);
  if (!authResponse) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product not ready');
  }
  const { studentId } = getBank;
  const accountDets = authResponse.numbers.ach[0];
  const accountId = authResponse.numbers.ach[0].account_id;
  const getAuthAccount = authResponse.accounts.find((acc) => acc.account_id === accountId);
  const insertBody = {
    studentId,
    accountNumber: accountDets.account,
    routingNumber: accountDets.routing,
    accountName: `Bank-${getAuthAccount.name}-${studentId}`.substring(0, 40),
    bankDispName: getAuthAccount.name,
    accountType: getAuthAccount.subtype.toUpperCase(),
    access_token: getBank.access_token,
  };
  await bankService.removeBanksByRequestId(getBank.requestId);
  const saveBank = await bankService.saveVerifiedBanks(studentId, insertBody);
  return saveBank;
};

const createSameDayMicroDepositLink = async (studentId, accessToken) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const linkTokenResponse = await plaidClient.createLinkToken({
    user: {
      client_user_id: studentId,
    },
    client_name: 'Sloan',
    country_codes: ['US'],
    language: 'en',
    webhook: 'http://54.176.67.107:3000/api/plaid/webhook_plaid',
    T: accessToken,
  });
  return linkTokenResponse;
};

const verifySameDayMicroDeposit = async (studentId, body) => {
  const authResponse = await retrieveAuth(body.accessToken);
  if (!authResponse) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product not ready');
  }
  const accountDets = authResponse.numbers.ach[0];
  const accountId = authResponse.numbers.ach[0].account_id;
  const getAuthAccount = authResponse.accounts.find((acc) => acc.account_id === accountId);
  const insertBody = {
    studentId,
    accountNumber: accountDets.account,
    routingNumber: accountDets.routing,
    accountName: `Bank-${accountDets.account}-${studentId}`.substring(0, 40),
    bankDispName: getAuthAccount.name,
    accountType: getAuthAccount.subtype.toUpperCase(),
    access_token: body.accessToken,
  };
  await bankService.removeBanksByRequestId(body.requestId);
  const saveBank = await bankService.saveVerifiedBanks(studentId, insertBody);
  return saveBank;
};

module.exports = {
  createLoanLinkToken,
  createBankLinkToken,
  exchangePublicToken,
  retrieveAuth,
  retrieveAccount,
  verifyAutomatedMicroDeposits,
  createSameDayMicroDepositLink,
  verifySameDayMicroDeposit,
};
