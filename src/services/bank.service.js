const httpStatus = require('http-status');
const Sila = require('sila-sdk').default;

const config = require('../config/config');
const ApiError = require('../utils/ApiError');
const userService = require('./user.service');
const silaService = require('./sila.service');
const { Bank } = require('../models');

const silaconfig = {
  handle: config.sila_staging.handle,
  key: config.sila_staging.key,
};
Sila.configure(silaconfig);

/**
 *
 *
 * Get Banks By StudentId
 *
 *
 */
const getBanks = async (studentId) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const response = await userService.getBanksById(studentId);
  return response;
};

/**
 *
 *
 * Get Banks By Item id
 *
 *
 */
const getBanksByItemId = async (itemId) => {
  const response = await Bank.findOne({ itemId });
  return response;
};

const removeBanksByRequestId = async (requestId) => {
  const response = await Bank.findOneAndDelete({ requestId });
  return response;
};

/**
 *
 *
 * Add Bank
 *
 *
 */
const addBanks = async (studentId, req) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const bankDetails = {
    userhandle: `${studentId}.silamoney.eth`,
    walletKey: await userService.decryptKeys(user.walletKey),
    accountNumber: req.account_num,
    routingNumber: req.account_routing,
    accountName: `Bank-${req.account_num}-${studentId}`.substring(0, 40),
    accountType: 'CHECKING',
  };
  const insertBody = {
    studentId,
    accountNumber: req.account_num,
    routingNumber: req.account_routing,
    accountName: `Bank-${req.account_name}-${studentId}`.substring(0, 40),
    bankDispName: req.account_name,
    accountType: 'CHECKING',
    access_token: req.access_token,
  };
  const response = await silaService.linkBankSila(bankDetails);
  if (!response) {
    throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, 'Bank could not be added.');
  }
  await Bank.create(insertBody);
  const getallbanks = await getBanks(studentId);
  return getallbanks;
};

/**
 *
 *
 * Save Verified Bank
 *
 *
 */
const saveVerifiedBanks = async (studentId, req) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const bankDetails = {
    userhandle: `${studentId}.silamoney.eth`,
    walletKey: await userService.decryptKeys(user.walletKey),
    accountNumber: req.accountNumber,
    routingNumber: req.routingNumber,
    accountName: req.accountName,
    accountType: req.accountType,
  };
  const insertBody = {
    studentId,
    accountNumber: req.accountNumber,
    routingNumber: req.routingNumber,
    accountName: req.accountName,
    bankDispName: req.bankDispName,
    accountType: req.accountType,
    access_token: req.access_token,
  };
  const response = await silaService.linkBankSila(bankDetails);
  if (!response) {
    throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, 'Bank could not be added.');
  }
  await Bank.create(insertBody);
  const getallbanks = await getBanks(studentId);
  return getallbanks;
};

/**
 *
 *
 * Exchange Public Token
 *
 *
 */
const saveBankPublicToken = async (studentId, req) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const insertBody = {
    studentId,
    accountNumber: req.accountId,
    itemId: req.item_id,
    requestId: req.request_id,
    access_token: req.access_token,
  };
  await Bank.create(insertBody);
  const getallbanks = await getBanks(studentId);
  return getallbanks;
};

const deleteBanks = async (studentId, accountName) => {
  const user = await userService.getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const wpk = await userService.decryptKeys(user.walletKey);
  const response = await silaService.deleteBankSila(studentId, accountName, wpk);
  if (!response.success) {
    throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, 'Bank Could Not be Deleted');
  }
  await Bank.findOneAndRemove(accountName);
  const getallbanks = await getBanks(studentId);
  return getallbanks;
};

module.exports = {
  getBanks,
  getBanksByItemId,
  removeBanksByRequestId,
  addBanks,
  saveVerifiedBanks,
  saveBankPublicToken,
  deleteBanks,
};
