const Dwolla = require('dwolla-v2').Client;
const config = require('../config/config');

const dwollaClient = new Dwolla({
  key: config.dwolla.sandbox.key,
  secret: config.dwolla.sandbox.secret,
  environment: 'sandbox', // defaults to 'production'
});

/**
 * Create Dwolla Verified User
 * @param {ObjectId} studentId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
// const accessToken = async (insertBody) => {
//   const apptoken = dwollaClient.auth.client();
//   return apptoken;
// };

const createVerifiedUser = async (insertBody) => {
  // console.log(DwollaClient);
  const user = dwollaClient.post('customers', insertBody);
  return user;
};

module.exports = {
  createVerifiedUser,
};
