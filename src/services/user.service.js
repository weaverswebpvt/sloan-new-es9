const httpStatus = require('http-status');
const { KmsKeyringNode, buildClient, CommitmentPolicy } = require('@aws-crypto/client-node');
const Sila = require('sila-sdk').default;
const Twilio = require('twilio');
const config = require('../config/config');
const { User, Otp, Loan, Bank } = require('../models');
const emailService = require('./email.service');
const ApiError = require('../utils/ApiError');
const logger = require('../config/logger');

const { accountSid, authToken } = { accountSid: config.twilio.accountSid, authToken: config.twilio.authToken }; // Your Account SID from www.twilio.com/console
const TwilioClient = new Twilio(accountSid, authToken);

const silaconfig = {
  handle: config.sila_staging.handle,
  key: config.sila_staging.key,
};
Sila.configure(silaconfig);

const { encrypt, decrypt } = buildClient(CommitmentPolicy.REQUIRE_ENCRYPT_REQUIRE_DECRYPT);
/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.EmailId)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  const user = await User.create(userBody);
  return user;
};

const signupViaSocialService = async (userBody) => {
  if (await User.isEmailTaken(userBody.EmailId)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  const user = await User.create(userBody);
  return user;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async (filter, options) => {
  const users = await User.paginate(filter, options);
  return users;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserById = async (studentId) => {
  return User.findOne({ studentId });
};

/**
 * Get Loans by Student id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getLoansById = async (studentId) => {
  return Loan.find({ studentId });
};

/**
 * Get Banks by Student id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getBanksById = async (studentId) => {
  return Bank.find({ studentId });
};

/**
 * Get user by object id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserByObjectId = async (id) => {
  return User.findOne({ _id: id });
};

/**
 * Check OTP exists by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getOtpByUserId = async (studentId) => {
  return Otp.findOne({ studentId });
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (EmailId) => {
  return User.findOne({ EmailId });
};

/**
 * Encrypt Keys
 * @param {string} data
 * @param {string} purpose
 * @returns {Promise<User>}
 */
const encryptKeys = async (data, purpose) => {
  if (!data) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data Not Provided');
  }
  const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
  const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
  const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
  const context = { stage: 'staging', purpose, origin: 'us-west-1' };
  const { result } = await encrypt(keyring, data, {
    encryptionContext: context,
  });
  return Buffer.from(result).toString('base64');
};

/**
 * Decrypt Keys
 * @param {string} data
 * @returns {Promise<User>}
 */
const decryptKeys = async (data) => {
  if (!data) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data Not Provided');
  }
  const generatorKeyId = 'arn:aws:kms:us-west-1:648672020807:alias/Sila_Encryption';
  const keyIds = ['arn:aws:kms:us-west-1:648672020807:key/d534598b-aaf3-443e-aee5-c51cc8b68ab1'];
  const keyring = new KmsKeyringNode({ generatorKeyId, keyIds });
  const decryptedDataRaw = Buffer.from(data, 'base64');
  const { plaintext } = await decrypt(keyring, decryptedDataRaw);
  return Buffer.from(plaintext).toString('ascii');
};

/**
 * Send Sms With Twilio
 * @param {string} data
 * @returns {Promise<User>}
 */
const TwilioSmsModule = async (request, code) => {
  if (!request) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Contact Number Not Provided');
  }
  if (!code) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Code Not Provided');
  }
  return TwilioClient.messages.create({ body: code, to: request, from: '+18563351378' });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

/**
 * Update user by id
 * @param {ObjectId} studentId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserByIdForAddress = async (studentId, updateBody) => {
  const wallet = await Sila.generateWallet();
  /* Encrypt the data. */
  const walletKey = await encryptKeys(wallet.privateKey, 'private_key');
  const walletAddress = await encryptKeys(wallet.address, 'wallet_address');

  const walletBody = {
    walletKey,
    walletAddress,
  };
  const user = await getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  Object.assign(user.address, updateBody);
  Object.assign(user, walletBody);
  await user.save();
  return user;
};

/**
 * Update user by id
 * @param {ObjectId} studentId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserByIdForBasicInfo = async (studentId, updateBody) => {
  const user = await getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

/**
 * Update SSN and USE SILA to Request KYC
 * @param {ObjectId} studentId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const verifySSN = async (studentId, req) => {
  const getdata = await getUserById(studentId);
  const { dob, fname, lname, address, EmailId, mobileNo } = getdata;
  const wpk = await decryptKeys(getdata.walletKey);
  const wad = await decryptKeys(getdata.walletAddress);
  const updateBody = { completionStatus: '5' };
  const updateBodyKYC = { isKYCVerified: true };
  // const wad = getdata.walletAddress;
  const userhandle = `${studentId}.silamoney.eth`;
  let reqKycResponse;
  let checkKycResponse;
  let phone;
  if (mobileNo.includes('+91')) {
    phone = mobileNo.substring(3);
  } else if (mobileNo.includes('+1')) {
    phone = mobileNo.substring(2);
  }
  // console.log(phone);
  const user = new Sila.User();
  logger.info('SILA USER >> %s', JSON.stringify(user));
  user.handle = userhandle;
  user.firstName = fname;
  user.lastName = lname;
  user.address = address.address1;
  user.city = address.city;
  user.state = address.state;
  user.zip = address.zip;
  user.country = 'US';
  user.phone = phone;
  user.email = EmailId;
  user.dateOfBirth = dob;
  user.ssn = req.ssn;
  user.cryptoAddress = wad;
  user.type = 'individual';
  // console.log(user);
  if (user !== undefined) {
    // const generatedWallet = {
    //   address: wad,
    //   privateKey: wpk,
    // };
    // console.log(generatedWallet);
    await Sila.register(user)
      .then(async (response) => {
        logger.info('User Response %s', JSON.stringify(response));
        if (response.statusCode !== 200) {
          throw new ApiError(httpStatus.NON_AUTHORITATIVE_INFORMATION, JSON.stringify(response));
        }
        reqKycResponse = await Sila.requestKYC(userhandle, wpk);
        // console.log(reqKycResponse);
        if (reqKycResponse.statusCode !== 200) {
          throw new ApiError(httpStatus.UNAUTHORIZED, JSON.stringify(reqKycResponse));
        }
      })
      .then(async () => {
        checkKycResponse = await Sila.checkKYC(userhandle, wpk);
        // console.log(checkKycResponse);
        if (checkKycResponse.statusCode !== 200) {
          throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, JSON.stringify(checkKycResponse));
        }
      })
      .then(async () => {
        if (!getdata) {
          throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
        }
        Object.assign(getdata, updateBody);
        Object.assign(getdata.Status, updateBodyKYC);
        await getdata.save();
      })
      .catch((err) => {
        throw new ApiError(httpStatus.NOT_FOUND, err);
      });
  }
  const addContact = await emailService.addContactSendgrid(
    fname,
    lname,
    EmailId,
    address.address1,
    address.city,
    address.state,
    address.zip,
    phone,
    studentId
  );
  if (!addContact) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Cannot Add to Sendgrid');
  }
  return getdata;
};

/**
 * Update Sila with NO KYC flow
 * @param {ObjectId} studentId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const registerNoKyc = async (studentId, body) => {
  const getdata = await getUserById(studentId);
  const wallet = await Sila.generateWallet();
  /* Encrypt the data. */
  const walletKey = await encryptKeys(wallet.privateKey, 'private_key');
  const walletAddress = await encryptKeys(wallet.address, 'wallet_address');

  const { EmailId } = getdata;
  let phone;
  if (body.phone.includes('+91')) {
    phone = body.phone.substring(3);
  } else if (body.phone.includes('+1')) {
    phone = body.phone.substring(2);
  }
  const updateBody = {
    fname: body.firstName,
    lname: body.lastName,
    dob: body.dob,
    EmailId,
    mobileNo: phone,
    walletKey,
    walletAddress,
    completionStatus: '5',
  };
  const updateAddress = {
    address1: body.address.address1,
    address2: body.address.address2,
    city: body.address.city,
    state: body.address.state,
    zip: body.address.zip,
  };
  const updateBodyKYC = { isKYCVerified: true };
  // const wad = getdata.walletAddress;
  const userhandle = `${studentId}.silamoney.eth`;
  let reqKycResponse;
  let checkKycResponse;
  // let phone;
  // if (mobileNo.includes('+91')) {
  //   phone = mobileNo.substring(3);
  // } else if (mobileNo.includes('+1')) {
  //   phone = mobileNo.substring(2);
  // }
  // console.log(phone);
  const user = new Sila.User();
  user.handle = userhandle;
  user.firstName = body.firstName;
  user.lastName = body.lastName;
  user.address = body.address.address1;
  user.city = body.address.city;
  user.state = body.address.state;
  user.zip = body.address.zip;
  user.email = EmailId;
  user.phone = body.phone;
  user.dateOfBirth = body.dob;
  user.cryptoAddress = wallet.address;
  // console.log(user);
  if (user !== undefined) {
    // const generatedWallet = {
    //   address: wad,
    //   privateKey: wpk,
    // };
    // console.log(generatedWallet);
    await Sila.register(user)
      .then(async () => {
        reqKycResponse = await Sila.requestKYC(userhandle, wallet.privateKey, 'NONE');
        // console.log(reqKycResponse);
        if (reqKycResponse.statusCode !== 200) {
          throw new ApiError(httpStatus.NOT_FOUND, JSON.stringify(reqKycResponse));
        }
      })
      .then(async () => {
        checkKycResponse = await Sila.checkKYC(userhandle, wallet.privateKey);
        // console.log(checkKycResponse);
        if (checkKycResponse.statusCode !== 200) {
          throw new ApiError(httpStatus.NOT_FOUND, checkKycResponse);
        }
      })
      .then(async () => {
        if (!getdata) {
          throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
        }
        Object.assign(getdata, updateBody);
        Object.assign(getdata.Status, updateBodyKYC);
        Object.assign(getdata.address, updateAddress);
        await getdata.save();
      })
      .catch((err) => {
        throw new ApiError(httpStatus.NOT_FOUND, err);
      });
  }
  const addContact = await emailService.addContactSendgrid(
    body.fname,
    body.lname,
    EmailId,
    body.address.address1,
    body.address.city,
    body.address.state,
    body.address.zip,
    body.phone,
    studentId
  );
  if (!addContact) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Cannot Add to Sendgrid');
  }
  return getdata;
};

/**
 * Get OTP Using Twilio Service
 * @param {string} data
 * @returns {Promise<User>}
 */
const sendOtp = async (studentId, contact) => {
  const user = await getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  let response;
  const otpExists = await getOtpByUserId(studentId);
  if (!otpExists) {
    const otpCreate = Math.floor(Math.random() * 999999 + 111111);
    const otp = `${otpCreate
      .toString()
      .substring(0, 4)} is your ( One Time Password )OTP for Sloan. Please do not share this OTP with anyone.`;
    const code = otpCreate.toString().substring(0, 4);
    const message = await TwilioSmsModule(contact.contact, otp);
    if (!message) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'Twilio Error while Sending Otp');
    }
    const otpBody = { studentId, contact: contact.contact, otp: code };
    response = await Otp.create(otpBody);
  } else {
    const otp = `${otpExists.otp} is your ( One Time Password )OTP for Sloan. Please do not share this OTP with anyone.`;
    await TwilioSmsModule(otpExists.contact, otp);
    response = otpExists;
  }
  return response;
};

/**
 * Verify OTP Using Twilio Service
 * @param {string} studentId
 * @param {Object} verifyBody
 * @returns {Promise<User>}
 */
const verifyOtp = async (studentId, verifyBody) => {
  const user = await getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const getOtp = await getOtpByUserId(studentId);
  const checkVerification = getOtp.otp === verifyBody.otp;
  if (!checkVerification) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'You have entered the wrong OTP. Please Try Again');
  } else if (getOtp.isVerified) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'You are already verified');
  } else {
    const updateOtp = {
      isVerified: true,
    };
    const updateUser = {
      mobileNo: verifyBody.mobileNo,
    };
    Object.assign(user, updateUser);
    await user.save();
    Object.assign(getOtp, updateOtp);
    await getOtp.save();
  }
  return user;
};

/**
 * Verify OTP Using Twilio Service
 * @param {string} studentId
 * @returns {Promise<User>}
 */
const turnNotificationsOn = async (studentId) => {
  const user = await getUserById(studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const checkNotification = user.Status.doNotifications;
  if (!checkNotification || checkNotification === undefined) {
    const updateUser = {
      doNotifications: true,
    };
    Object.assign(user.Status, updateUser);
    await user.save();
  } else {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Notifications Already on');
  }
  return user;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await user.remove();
  return user;
};

module.exports = {
  createUser,
  signupViaSocialService,
  queryUsers,
  getUserById,
  getLoansById,
  getBanksById,
  getUserByObjectId,
  updateUserById,
  getOtpByUserId,
  getUserByEmail,
  encryptKeys,
  decryptKeys,
  TwilioSmsModule,
  updateUserByIdForAddress,
  updateUserByIdForBasicInfo,
  verifySSN,
  registerNoKyc,
  sendOtp,
  verifyOtp,
  turnNotificationsOn,
  deleteUserById,
};
