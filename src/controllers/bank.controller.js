const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { bankService } = require('../services');
const { bankResponse } = require('../validations/response.validation');
const messages = require('../utils/responseMessages');

const getBanks = catchAsync(async (req, res) => {
  const user = await bankService.getBanks(req.params.studentId);
  await messages('getBanks').then((msgResponse) =>
    bankResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const addBanks = catchAsync(async (req, res) => {
  const user = await bankService.addBanks(req.params.studentId, req.body);
  await messages('bankAdded').then((msgResponse) =>
    bankResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const deleteBanks = catchAsync(async (req, res) => {
  const user = await bankService.deleteBanks(req.params.studentId, req.body.accountName);
  await messages('bankDeleted').then((msgResponse) =>
    bankResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

module.exports = {
  getBanks,
  addBanks,
  deleteBanks,
};
