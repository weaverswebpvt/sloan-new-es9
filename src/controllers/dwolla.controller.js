const catchAsync = require('../utils/catchAsync');
const { dwollaService } = require('../services');

const createVerifiedUser = catchAsync(async (req, res) => {
  const user = await dwollaService.createVerifiedUser(req.body);
  // console.log('User Details > ', user.headers);
  res.send(user.headers.get('location'));
});

module.exports = {
  createVerifiedUser,
};
