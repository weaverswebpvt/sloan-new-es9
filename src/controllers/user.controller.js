const httpStatus = require('http-status');
const OneSignal = require('onesignal-node');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { tokenService, userService } = require('../services');
const { generalResponse, fullResponseWithToken } = require('../validations/response.validation');
const messages = require('../utils/responseMessages');

const notificationClient = new OneSignal.Client(
  'f881d3e7-8048-4996-9bb1-4fb23334329b',
  'NTdmY2Q3ODEtOTBhOS00YjI3LWJmMzYtN2QzZWIzNWIxYjEy'
);

const createUser = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  await messages('createUser').then((msgResponse) =>
    generalResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const getUsers = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await userService.queryUsers(filter, options);
  await messages('getUsers').then((msgResponse) =>
    generalResponse(result, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const internalLogin = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.params.studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const tokens = await tokenService.generateAuthTokens(user);
  await messages('internalLogin').then((msgResponse) =>
    fullResponseWithToken(user, tokens, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const getUser = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.params.studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await messages('getUser').then((msgResponse) =>
    generalResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const saveAddress = catchAsync(async (req, res) => {
  const user = await userService.updateUserByIdForAddress(req.params.studentId, req.body);
  await messages('addressStored').then((msgResponse) =>
    generalResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const saveBasicInfo = catchAsync(async (req, res) => {
  const user = await userService.updateUserByIdForBasicInfo(req.params.studentId, req.body);
  await messages('basicInfoStored').then((msgResponse) =>
    generalResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const verifySSN = catchAsync(async (req, res) => {
  const user = await userService.verifySSN(req.params.studentId, req.body);
  await messages('ssnVerified').then((msgResponse) =>
    generalResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const registerNoKyc = catchAsync(async (req, res) => {
  const user = await userService.registerNoKyc(req.params.studentId, req.body);
  await messages('ssnVerified').then((msgResponse) =>
    generalResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const sendOtp = catchAsync(async (req, res) => {
  const user = await userService.sendOtp(req.params.studentId, req.body);
  await messages('otpSent').then((msgResponse) =>
    generalResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const verifyOtp = catchAsync(async (req, res) => {
  const user = await userService.verifyOtp(req.params.studentId, req.body);
  await messages('phoneVerified').then((msgResponse) =>
    generalResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const turnNotificationsOn = catchAsync(async (req, res) => {
  const user = await userService.turnNotificationsOn(req.params.studentId);
  await messages('notificationsOn').then((msgResponse) =>
    generalResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const addDeviceData = catchAsync(async (req, res) => {
  const response = await notificationClient.addDevice({
    device_type: 1,
    identifier: 'f98e-2288',
  });
  res.send(response);
});

const createNotification = catchAsync(async (req, res) => {
  const notification = {
    contents: { en: 'TYEST TES TJHKJ GJUGH KJOIKJH OK' },
    include_player_ids: ['9e9825ee-5ae0-4197-ab5e-f4bd1e161dec'],
  };

  // using async/await
  const response = await notificationClient.createNotification(notification);
  res.send(response);
});

const deleteUser = catchAsync(async (req, res) => {
  await userService.deleteUserById(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createUser,
  getUsers,
  internalLogin,
  getUser,
  saveAddress,
  saveBasicInfo,
  verifySSN,
  registerNoKyc,
  sendOtp,
  verifyOtp,
  turnNotificationsOn,
  addDeviceData,
  createNotification,
  deleteUser,
};
