const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { loanService } = require('../services');
const { loanResponse } = require('../validations/response.validation');
const messages = require('../utils/responseMessages');

const getLoans = catchAsync(async (req, res) => {
  const user = await loanService.getLoans(req.params.studentId);
  await messages('getLoans').then((msgResponse) =>
    loanResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const addLoans = catchAsync(async (req, res) => {
  const user = await loanService.addLoans(req.params.studentId, req.body.loandata);
  await messages('loanAdded').then((msgResponse) =>
    loanResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const deleteLoans = catchAsync(async (req, res) => {
  const user = await loanService.deleteLoans(req.params.studentId, req.body.accountId);
  await messages('loanDeleted').then((msgResponse) =>
    loanResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

module.exports = {
  getLoans,
  addLoans,
  deleteLoans,
};
