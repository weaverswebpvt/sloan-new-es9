const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { silaService, userService } = require('../services');
const ApiError = require('../utils/ApiError');

// START SILA MANUAL CHECKS

const decryptData = catchAsync(async (req, res) => {
  const decryptedData = await userService.decryptKeys(req.body.data);
  res.status(httpStatus.OK).send(decryptedData);
});

const checkHandle = catchAsync(async (req, res) => {
  const data = await silaService.checkHandle(req.body.studentId);
  res.status(httpStatus.OK).send(data);
});

const checkkyc = catchAsync(async (req, res) => {
  const data = await silaService.checkkyc(req.body);
  res.status(httpStatus.OK).send(data);
});

const updateKyc = catchAsync(async (req, res) => {
  const data = await silaService.updateKyc(req.body);
  res.status(httpStatus.OK).send(data);
});

const updatePhone = catchAsync(async (req, res) => {
  const data = await silaService.updatePhone(req.body);
  res.status(httpStatus.OK).send(data);
});

const getStudent = catchAsync(async (req, res) => {
  const data = await silaService.getStudent(req.body);
  res.status(httpStatus.OK).send(data);
});

const getTransactions = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.body.studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const wpk = await userService.decryptKeys(user.walletKey);
  const data = await silaService.getTransactions(req.body.studentId, wpk);
  res.status(httpStatus.OK).send(data);
});

const deleteTransaction = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.body.studentId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const wpk = await userService.decryptKeys(user.walletKey);
  const data = await silaService.deleteTransaction(req.body.studentId, wpk, req.body.tid);
  res.status(httpStatus.OK).send(data);
});

module.exports = {
  decryptData,
  checkHandle,
  checkkyc,
  updateKyc,
  updatePhone,
  getStudent,
  getTransactions,
  deleteTransaction,
};
