const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { authService, userService, tokenService, emailService } = require('../services');
const { fullResponseWithToken, fullResponse, generalResponse } = require('../validations/response.validation');
const messages = require('../utils/responseMessages');

const register = catchAsync(async (req, res) => {
  const customToken = new Date().getTime();
  const studentId = await tokenService.autoidmodule();
  const d = new Date();
  const timeAnddate = d.toISOString();
  const insertquery = {
    studentId,
    EmailId: req.body.email,
    password: req.body.password,
    'Status.signedUpVia': 'Manual',
    salt: customToken,
    memberSince: timeAnddate,
    isLoggedIn: true,
  };
  const user = await userService.createUser(insertquery);
  const tokens = await tokenService.generateAuthTokens(user);
  await messages('createUser').then((msgResponse) =>
    fullResponseWithToken(user, tokens, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const signupViaSocial = catchAsync(async (req, res) => {
  const customToken = new Date().getTime();
  const studentId = await tokenService.autoidmodule();
  const d = new Date();
  const timeAnddate = d.toISOString();
  const userobj = {
    firstnameFromSocial: req.body.familyName ? req.body.familyName : req.body.fullName.familyName,
    lastnameFromSocial: req.body.givenName ? req.body.givenName : req.body.fullName.givenName,
    emailFromSocial: req.body.email ? req.body.email : req.body.email,
    photo: req.body.photo ? req.body.photo : '',
    googleProvidedId: req.body.id ? req.body.id : req.body.user,
    via: req.body.id ? 'GOOGLE' : 'APPLE',
  };
  const insertquery = {
    studentId,
    EmailId: userobj.emailFromSocial,
    fname: userobj.firstnameFromSocial,
    lname: userobj.lastnameFromSocial,
    'Status.signedUpVia': userobj.via,
    'Status.socialId': userobj.googleProvidedId,
    profilePic: userobj.photo,
    memberSince: timeAnddate,
    salt: customToken,
    isLoggedIn: true,
  };
  const user = await userService.signupViaSocialService(insertquery);
  const tokens = await tokenService.generateAuthTokens(user);
  await messages('createUser').then((msgResponse) =>
    fullResponseWithToken(user, tokens, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  const user = await authService.loginUserWithEmailAndPassword(email, password);
  const tokens = await tokenService.generateAuthTokens(user);
  await messages('createUser').then((msgResponse) =>
    fullResponseWithToken(user, tokens, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const logout = catchAsync(async (req, res) => {
  await authService.logout(req.body.refreshToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const refreshTokens = catchAsync(async (req, res) => {
  const tokens = await authService.refreshAuth(req.body.refreshToken);
  res.send({ ...tokens });
});

const forgotPassword = catchAsync(async (req, res) => {
  const resetPasswordToken = await tokenService.generateResetPasswordToken(req.body.email);
  await emailService.sendResetPasswordEmail(req.body.email, 'Reset Password', resetPasswordToken);
  await messages('forgotPassword').then((msgResponse) =>
    fullResponse(resetPasswordToken, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const getUserById = catchAsync(async (req, res) => {
  const userdata = await authService.getUserById(req.body.studentId);
  res.status(httpStatus.CREATED).send({ userdata });
});

const resetPassword = catchAsync(async (req, res) => {
  const user = await authService.resetPassword(req.query.token, req.body.password);
  // logger.info(test);
  await messages('resetPassword').then((msgResponse) =>
    generalResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

module.exports = {
  register,
  signupViaSocial,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  getUserById,
  resetPassword,
};
