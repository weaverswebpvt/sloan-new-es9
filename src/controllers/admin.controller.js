/* eslint-disable */
const httpStatus = require('http-status');
const OneSignal = require('onesignal-node');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { userService } = require('../services');
const { generalResponse } = require('../validations/response.validation');
const messages = require('../utils/responseMessages');

const getUsers = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await userService.queryUsers(filter, options);
  await messages('getUsers').then((msgResponse) =>
    generalResponse(result, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

module.exports = {
  getUsers,
};
