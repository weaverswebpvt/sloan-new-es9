const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { paymentService } = require('../services');
const { fullResponse } = require('../validations/response.validation');
const messages = require('../utils/responseMessages');

const payLoanBank = catchAsync(async (req, res) => {
  const payment = await paymentService.payLoanBank(req.params.studentId, req.body);
  await messages('paymentMade').then((msgResponse) =>
    fullResponse(payment, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

const payLoanWallet = catchAsync(async (req, res) => {
  const user = await paymentService.payLoanWallet(req.params.studentId, req.body);
  await messages('bankpaymentMadeAdded').then((msgResponse) =>
    fullResponse(user, httpStatus.OK, msgResponse).then((response) => res.send(response))
  );
});

module.exports = {
  payLoanBank,
  payLoanWallet,
};
