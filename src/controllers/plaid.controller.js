const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { plaidService } = require('../services');
// const messages = require('../utils/responseMessages');

const createLoanLinkToken = catchAsync(async (req, res) => {
  const linkTokenResponse = await plaidService.createLoanLinkToken(req.params.studentId);
  res.status(httpStatus.OK).send(linkTokenResponse);
});

const createBankLinkToken = catchAsync(async (req, res) => {
  const linkTokenResponse = await plaidService.createBankLinkToken(req.params.studentId);
  res.status(httpStatus.OK).send(linkTokenResponse);
});

const exchangePublicToken = catchAsync(async (req, res) => {
  const linkTokenResponse = await plaidService.exchangePublicToken(req.params.studentId, req.body);
  res.status(httpStatus.OK).send(linkTokenResponse);
});

const createSameDayMicroDepositLink = catchAsync(async (req, res) => {
  const linkTokenResponse = await plaidService.createSameDayMicroDepositLink(req.params.studentId, req.body.accessToken);
  res.status(httpStatus.OK).send(linkTokenResponse);
});

const verifySameDayMicroDeposit = catchAsync(async (req, res) => {
  const linkTokenResponse = await plaidService.verifySameDayMicroDeposit(req.params.studentId, req.body);
  res.status(httpStatus.OK).send(linkTokenResponse);
});

module.exports = {
  createLoanLinkToken,
  createBankLinkToken,
  exchangePublicToken,
  createSameDayMicroDepositLink,
  verifySameDayMicroDeposit,
};
