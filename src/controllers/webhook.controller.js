const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { userService, emailService } = require('../services');
// const { plaidService } = require('../services');
const ApiError = require('../utils/ApiError');
const logger = require('../config/logger');

const plaidWebhook = catchAsync(async (req) => {
  // let webhookResponse = '';
  // const data = req.body.message;
  // if (data.webhook_code === 'AUTOMATICALLY_VERIFIED') {
  //   await plaidService.verifyAutomatedMicroDeposits(data);
  // }
  // res.status(httpStatus.OK).send(200);
  logger.info(req.body);
});

const silaKYCWebhook = catchAsync(async (req, res) => {
  const eventtype = req.body.event_type;
  const status = req.body.event_details.outcome;
  const getid = req.body.event_details.entity;
  const studentId = getid.replace('id', 'Id');
  const getdata = await userService.getUserById(studentId);
  if (!getdata) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  if (eventtype === 'kyc') {
    if (status === 'passed') {
      // global.io.emit('kycverification', { kyc: req.body });
      const updateBodyKYC = { isKYCVerified: true };

      // const dynamicTemplateData = {
      //   name: getdata.fname + ' ' + getdata.lname,
      // };
      await emailService.sendEmailWithSendgrid(
        getdata.EmailId,
        'Welcome to SLOAN',
        'WELCOME to SLOAN, We are very pleased to hear from you'
      );
      Object.assign(getdata.Status, updateBodyKYC);
      await getdata.save();
      res.status(httpStatus.OK).send(200);
    } else {
      // global.io.emit('kycpending', { kyc: req.body });
    }
  }
});

module.exports = {
  plaidWebhook,
  silaKYCWebhook,
};
