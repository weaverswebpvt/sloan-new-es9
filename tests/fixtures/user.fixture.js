const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const faker = require('faker');
const User = require('../../src/models/user.model');

const password = 'password1';
const salt = bcrypt.genSaltSync(8);
const hashedPassword = bcrypt.hashSync(password, salt);

const userOne = {
  _id: mongoose.Types.ObjectId(),
  name: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  password,
  role: 'user',
};

const socialUserOne = {
  _id: mongoose.Types.ObjectId(),
  name: faker.name.findName(),
  EmailId: faker.internet.email().toLowerCase(),
  studentId: faker.lorem.word(),
  fname: faker.name.firstName(),
  lname: faker.name.lastName(),
  salt,
  isLoggedIn: faker.lorem.word(),
  loanDetails: faker.random.arrayElement(),
  memberSince: faker.lorem.word(),
  mobileNo: faker.lorem.word(),
  profilePic: faker.image.imageUrl(),
  roundUps: faker.random.arrayElement(),
  bankAccounts: faker.random.arrayElement(),
  dob: faker.lorem.word(),
  address: faker.random.arrayElement(),
  activities: faker.random.arrayElement(),
  Status: faker.random.arrayElement(),
};

const userTwo = {
  _id: mongoose.Types.ObjectId(),
  name: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  password,
  role: 'user',
};

const admin = {
  _id: mongoose.Types.ObjectId(),
  name: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  password,
  role: 'admin',
};

const insertUsers = async (users) => {
  await User.insertMany(users.map((user) => ({ ...user, password: hashedPassword })));
};

module.exports = {
  userOne,
  socialUserOne,
  userTwo,
  admin,
  insertUsers,
};
