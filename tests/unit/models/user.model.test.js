const faker = require('faker');
const { User } = require('../../../src/models');

describe('User model', () => {
  describe('User validation', () => {
    let newUser;
    beforeEach(() => {
      newUser = {
        name: faker.name.findName(),
        EmailId: faker.internet.email().toLowerCase(),
        studentId: faker.lorem.word(),
        fname: faker.name.firstName(),
        lname: faker.name.lastName(),
        salt: faker.lorem.word(),
        isLoggedIn: faker.lorem.word(),
        loanDetails: faker.random.arrayElement(),
        memberSince: faker.lorem.word(),
        mobileNo: faker.lorem.word(),
        profilePic: faker.image.imageUrl(),
        roundUps: faker.random.arrayElement(),
        bankAccounts: faker.random.arrayElement(),
        dob: faker.lorem.word(),
        address: faker.random.arrayElement(),
        activities: faker.random.arrayElement(),
        Status: faker.random.arrayElement(),
      };
    });

    test('should correctly validate a valid user', async () => {
      await expect(new User(newUser).validate()).resolves.toBeUndefined();
    });

    test('should throw a validation error if email is invalid', async () => {
      newUser.EmailId = 'invalidEmail';
      await expect(new User(newUser).validate()).rejects.toThrow();
    });
  });

  describe('User toJSON()', () => {
    test('should not return user password when toJSON is called', () => {
      const newUser = {
        name: faker.name.findName(),
        EmailId: faker.internet.email().toLowerCase(),
        studentId: faker.lorem.word(),
        fname: faker.name.firstName(),
        lname: faker.name.lastName(),
        salt: faker.lorem.word(),
        isLoggedIn: faker.datatype.boolean(),
        loanDetails: faker.random.arrayElement(),
        memberSince: faker.lorem.word(),
        mobileNo: faker.lorem.word(),
        profilePic: faker.image.imageUrl(),
        roundUps: faker.random.arrayElement(),
        bankAccounts: faker.random.arrayElement(),
        dob: faker.lorem.word(),
        address: faker.random.arrayElement(),
        activities: faker.random.arrayElement(),
        Status: faker.random.arrayElement(),
      };
      expect(new User(newUser).toJSON()).not.toHaveProperty('password');
    });
  });
});
